<?php 

header('Content-type: text/plain');

$filename = 'param.json';

if (file_exists($filename)){
	$string = file_get_contents($filename);
	$json = json_decode($string, true);
} else {
	echo 'File param.json not found';
	exit;
}

if (isset($_GET['botonly'])) $json['botonly'] = $_GET['botonly'];
if (isset($_GET['weather'])) {
	if ($_GET['weather'] === 'napbot') {
		$json['forced'] = 'false';
		$json['weather'] = null;
	} else {
		$json['forced'] = 'true';
		$json['weather'] = $_GET['weather'];
	}
}

foreach ($json as $k => $v) {
	echo $k, " : ", $v, "\n";
}

$fp = fopen($filename, 'w');
fwrite($fp, json_encode($json));
fclose($fp);

?>
