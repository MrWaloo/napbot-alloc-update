<?php

/**
 * DYNAMIC NAPBOTS ALLOCATION
 * (beta version, please double-check if its working properly)
 * This work is a fork of napbots-weather-allocations from thibault-ketterer
 * adapted for google cloud deployement by Fred.Dupont
 */

// Account details
$email = 'napbot_email';
$password = 'napbot_password';
$botOnly_cfg = false; // true or false

// Weather dependent compositions
//  - Total of allocations should be equal to 1
//  - Leverage should be between 0.00 and 1.50
//  - How to find bot IDS: https://imgur.com/a/ayit9pR


$compositions = [
	'mild_bear' => [
		'compo' => [
			'STRAT_BTC_USD_FUNDING_8H_1' => 0.15,
			'STRAT_ETH_USD_FUNDING_8H_1' => 0.15,
			'STRAT_ETH_USD_H_4_V2' => 0.15,
			'STRAT_BTC_USD_H_4_V2' => 0.15,
			'STRAT_BTC_ETH_USD_H_1' => 0.40,
		],
		'leverage' => 1.0,
	],
	'mild_bull' => [
		'compo' => [
			'STRAT_BTC_ETH_USD_LO_H_1' => 0.50,
			'STRAT_BTC_ETH_USD_H_1' => 0.50,
		],
		'leverage' => 1.5,
	],
	'extreme' => [
		'compo' => [
			'STRAT_ETH_USD_H_3_V2' => 0.35,
			'STRAT_BTC_USD_H_3_V2' => 0.35,
			'STRAT_BTC_ETH_USD_H_1' => 0.30,
		],
		'leverage' => 0.5,
	]
];


/*
 * Some bot ids on 2021-01-25

--------------- Hourly -------------------
 
STRAT_BTC_ETH_USD_H_1 => NapoX alloc ETH/BTC/USD AR hourly
STRAT_BTC_USD_FUNDING_8H_1 => NapoX BTC Funding AR hourlyY
STRAT_BTC_ETH_USD_LO_H_1 => NapoX alloc ETH/BTC/USD LO hourly
STRAT_ETH_USD_FUNDING_8H_1 => NapoX ETH Funding AR hourly
STRAT_ETH_USD_H_3_V2 => NapoX ETH Ultra flex AR hourly
STRAT_BTC_USD_H_3_V2 => NapoX BTC Ultra flex AR hourly
STRAT_ETH_USD_H_4_V2 => NapoX ETH AR hourly
STRAT_BTC_USD_H_4_V2 => NapoX BTC AR hourly

--------------- Daily -------------------

STRAT_BTC_ETH_USD_LO_D_1 = NapoX alloc ETH/BTC/USD LO daily
STRAT_BTC_ETH_USD_D_1_V2 = NapoX alloc ETH/BTC/USD AR daily
STRAT_BCH_USD_LO_D_1 = NapoX BCH LO daily


*/

?>
